/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {getHotelsFromApi} from './src/Service/HotelsApi';
import Icon from 'react-native-vector-icons/FontAwesome';
import MainView from './src/Screens/MainView';
import DetailView from './src/Screens/DetailView';
import { Router, Scene } from 'react-native-router-flux';




export default class App extends Component {
  constructor(props){
    super(props);
    // getHotelsFromApi().then(response=>{
    //   console.log(response);
    // });
  }
  render() {
    return (
      <Router>
        <Scene>
          <Scene
            key="mainview"
            component={MainView}
            title="Hoteles"
            initial={true}
          />
          <Scene
            key="detailView"
            component={DetailView}
            title="Detalle"
          />
        </Scene>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
