async function getHotelsFromApi(search) {
	search = (typeof search=='undefined' || search== null|| search.toString().trim().length<=0)?"":search;
	try {
		let response = await fetch(`http://192.168.0.11:3000/hotels/${search}`);
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(error);
	}
}

async function getHotelById(search) {
	search = search || "";
	try {
		let response = await fetch(`http://192.168.0.11:3000/hotel/${search}`);
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(error);
	}
}
module.exports =  {getHotelsFromApi,getHotelById};

