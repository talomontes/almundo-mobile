import React, { Component} from 'react';
import { View, TouchableOpacity, TextInput, StyleSheet, Image, Text, FlatList} from 'react-native';
import HotelPrice from '../Components/HotelItems/HotelPrice';
import { HotelServices } from '../Components/HotelItems/HotelServices';
import Icon from 'react-native-vector-icons/FontAwesome';
import { getHotelById } from '../Service/HotelsApi';


export default class HotelDetail extends Component {
	constructor(props) {
        super(props);
		this.state = {
            nuId: this.props.nuId,
            obHotelInfo: {stars:0}
		};
		this.GetHotelsById = this.GetHotelsById.bind(this);
        this.GetHotelsById(this.props.nuId);
	}

	render() {
		return (
			<View>
				<Image
					style={{ width: 50, height: 50 }}
					source={{ uri: this.state.obHotelInfo.images }} />
					<View  style={styles.bottomView}>
						<FlatList
							data={this.state.obHotelInfo.stars}
							renderItem={({ item, index }) =>
								<Icon
									name="star"
									size={20}
									color="yellow"
									style={styles.stars}
								/>
							}
							keyExtractor={(item, index) => index}
							style={styles.starsList}
						/>
					</View>
				{/* <View>
					<HotelServices arServices={this.state.obHotelInfo.arServices} ></HotelServices>
				</View>  */}

				<View>
                    <Text>{this.state.obHotelInfo.description}</Text>
				</View>
				<View>
					<HotelPrice price={this.state.obHotelInfo.price}></HotelPrice>
					<TouchableOpacity >
						<Text>
							"Reservar"
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
    }
    
    GetHotelsById(nuHotelId){
		getHotelById(nuHotelId).then(obHotel => {
			console.log('Aqui el hotel:', obHotel)
			this.setState({obHotelInfo: obHotel});
		});
	}
	ReservarHotel(){
		console.log('RESERVAR');
	}


}

const styles = StyleSheet.create({
	item:{
		width:'100%',
		padding: 5,
		backgroundColor: 'white',
		marginBottom: 3,
		borderRadius: 5
	},
	mainView: {
		flexDirection: 'row',
		flex:1,
		
	},
	valuesView:{
		flexDirection: 'column',
		flex:1,
		marginLeft: 3
	},
	price:{
		flex:1,
		width:'100%',
	},
	stars:{
		flex:1,
		width:'100%',
	},
	nameText:{
		flex:0.75,
		fontSize: 30
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row'
	},
	starsList: {
		flex: 1,
		flexDirection: 'row',
		width: '100%'
	},
	stars: {
		margin: 2
	}
});