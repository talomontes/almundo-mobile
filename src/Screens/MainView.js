import React, { Component} from 'react'
import { View, TouchableOpacity, TextInput, StyleSheet} from 'react-native'
import HotelList from '../Components/HotelList/HotelList'
import { getHotelsFromApi } from '../Service/HotelsApi'
import Icon from 'react-native-vector-icons/FontAwesome'


export default class MainView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			sbSearch: "",
			arHotels: []
		};
		this.GetHotelsData();
	}
	render() {
		return (
			<View style={styles.mainView}>
				<View style={styles.container}>
					<TextInput
						style={styles.input}
						onChangeText={(text) => this.setState({ sbSearch: text })}
						value={this.state.text}
					/>
					<TouchableOpacity style={styles.butonIcon} onPress={(this.onPressButton.bind(this))} >
						<Icon name="search" size={30} color="#900" />
					</TouchableOpacity>
				</View>
				<View style={styles.listView}>
					<HotelList arHotels={this.state.arHotels} styles={styles.hotelList}></HotelList>
				</View>
			</View>
		);
	}
	onPressButton(){
		getHotelsFromApi(this.state.sbSearch).then(arHotels => {
			this.setState({arHotels: arHotels});
		});
	}
	GetHotelsData(){
		getHotelsFromApi().then(arHotels => {
			this.setState({arHotels: arHotels});
		});
	}
}

const styles = StyleSheet.create({
	mainView:{
		alignItems:'flex-start',
		flexDirection:'column',
		flex:1
	},
	container: {
        backgroundColor: '#ffffff',
		alignItems: 'center',
        marginBottom: 4,
        flex: 0.10,
        flexDirection: 'row',
        elevation: 1,
		borderRadius: 2,
		justifyContent: 'center'
	},
	butonIcon: {
		flex:1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
		// marginTop: 13,
	},
    input:{
        flex:5,
		fontSize:13,
		color: '#EE3209'
	},
	hotelList: {
		flex:1,
		flexDirection: 'column'
	},
	listView: {
		flex:0.90,
		width:'100%'
	},
		starsList: {
		flex:1,
		flexDirection: 'row',
		width:'100%'
	},
	stars:{
		margin: 2
	}
})