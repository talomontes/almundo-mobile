import React, { Component } from 'react'
import { View, Text, Image,TouchableOpacity } from 'react-native'
import HotelPrice from './HotelPrice'
import { HotelStars } from './HotelStars'
import { HotelServices } from './HotelServices'
import Icon from 'react-native-vector-icons/FontAwesome';


export default class HotelDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentWillMount() {
		this.setState({obHotelInfo: this.props.obHotelInfo});
	}
	componentDidMount(){
		this.setState({});
	}
	render() {
		return (
			<View>
				{/* <Text>
					{this.props.obHotelInfo.name}
				</Text> */}
				<Image
					style={{ width: 50, height: 50 }}
					source={{ uri: this.state.obHotelInfo.image }} />
				<HotelStars hotelStars={this.state.obHotelInfo.nuStars} />
				<View>
					<HotelServices arServices={this.state.obHotelInfo.arServices} ></HotelServices>
				</View>
				<View>
					<Text>{this.state.obHotelInfo.description}</Text>
				</View>
				<View>
					<HotelPrice price={this.state.obHotelInfo.price}></HotelPrice>
					<TouchableOpacity onPress={this.ReservarHotel()}>
						<Text>
							"Reservar"
						</Text>
					</TouchableOpacity>
				</View>
			</View>

		);
	}

	ReservarHotel(){
		console.log('RESERVAR');
	}


}