import React, { Component } from 'react'
import { View, Image, FlatList, StyleSheet } from 'react-native'
import HotelItem from '../HotelItems/HotelItem'


export default class HotelList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			list: this.props.arHotels
		}
	}
	componentWillMount() {
		this.setState({list: this.props.arHotels});
	}
	componentWillUpdate(nextProps, nextState){
		if(this.state.list!=nextProps.arHotels){
			this.setState({list: nextProps.arHotels});
		}
	}
	
	render() {
		return (
			<View style={styles.listView}>
				<FlatList
					data={this.state.list}
					renderItem={({ item, index }) =>
						<HotelItem obHotelInfo={item}></HotelItem>
					}
					keyExtractor={(item, index) => index}
				/>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	listView: {
		flex:1,
		flexDirection: 'row',
		paddingRight: 3,
		paddingLeft: 3
	}
});

export { HotelList };