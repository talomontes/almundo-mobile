import {View, StatusBar, Navigator } from 'react-native'

export default class ToolBar extends Components {

	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return (
			<View>
				<StatusBar
					backgroundColor="blue"
					barStyle="light-content"
				/>
				<Navigator
					initialRoute={{ statusBarHidden: false }}
					renderScene={(route, navigator) =>
						<View>
							<StatusBar hidden={route.statusBarHidden}/>
						</View>
					}
				/>
			</View>
		);
	}
}

