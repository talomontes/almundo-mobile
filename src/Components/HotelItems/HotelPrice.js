import React, { Component} from 'react'
import { View,Text,Image,StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class HotelPrice extends Component {
	constructor(props) {
	  super(props);
	  this.state = {};
	}
	componentWillUpdate(nextProps, nextState){
		if(this.state.price!=nextProps.price){
			this.setState({price: nextProps.price});
		}
	}
    render() {
        return (
        	<View style={styles.price}>
				{/* <View style={styles.valueView}> */}
					<Text style={styles.simbolText}>COP </Text>
					<Text style={styles.priceText}>
						{this.state.price}
					</Text>
				{/* </View> */}
				{/* <View> */}
				<Icon
					name="moon-o"
					size={20}
					color="#900"
					style={styles.moon}
				/>
				{/* </View> */}
            </View>
        );
    }	
}

const styles = StyleSheet.create({

	price:{
		flexDirection:'row',
		flex:1,
		width:'100%',
		// justifyContent:'center',
		alignItems:'flex-end',
		justifyContent: 'space-between',
		// alignItems: 'flex-end'
	},
	valueView: {
		flexDirection:'row',
		flex:0.75,
		// width:'100%',
		justifyContent: 'flex-end',
		alignItems: 'flex-end'
	},
	simbolText:{
		fontSize:20,
		alignItems: 'flex-end'
	},
	priceText: {
		fontSize:30,
		lineHeight:24
		// alignItems: 'flex-end'
	},
	moon: {
		flex:0.25,
		marginLeft:2
	}
});