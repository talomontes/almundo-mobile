import React, { Component } from 'react'
import { View, Text, Image,TouchableOpacity,StyleSheet} from 'react-native'
import HotelPrice from './HotelPrice'
import { HotelStars } from './HotelStars'
import { Actions } from 'react-native-router-flux';


export default class HotelItem extends Component {
	constructor(props) {
		super(props);
		// this.props.text = 'argentino';
		this.state = {
			obHotelInfo: this.props.obHotelInfo
		};
	}
	componentWillUpdate(nextProps, nextState){
		if(this.state.obHotelInfo!=nextProps.obHotelInfo){
			this.setState({obHotelInfo: nextProps.obHotelInfo});
		}
	}

	render() {
		return (
			<TouchableOpacity
				onPress={(this.onPressButton.bind(this))} 
				style={styles.item}>
				<View style={styles.mainView}>
					<Image
						style={{ width: 100, height: 100 }}
						source={{ uri: this.state.obHotelInfo.images }} />
					<View style={styles.valuesView}>
						<Text style={styles.nameText}>
							{this.state.obHotelInfo.name}
						</Text>
						<View  style={styles.bottomView}>
							<HotelStars style={styles.stars} hotelStars={this.state.obHotelInfo.stars} />
							<HotelPrice style={styles.price} price={this.state.obHotelInfo.price} ></HotelPrice>
						</View>
					</View>
				</View>
			</TouchableOpacity>

		);
	}
	onPressButton(){
		Actions.detailView({nuId:this.state.obHotelInfo._id,title:this.state.obHotelInfo.name});
	}

}
const styles = StyleSheet.create({
	item:{
		// flex:1,
		width:'100%',
		padding: 5,
		backgroundColor: 'white',
		marginBottom: 3,
		borderRadius: 5
	},
	mainView: {
		flexDirection: 'row',
		flex:1,
		
	},
	valuesView:{
		flexDirection: 'column',
		flex:1,
		marginLeft: 3
	},
	price:{
		flex:1,
		width:'100%',
	},
	stars:{
		flex:1,
		width:'100%',
	},
	nameText:{
		flex:0.75,
		fontSize: 30
	},
	bottomView: {
		flex: 0.25,
		flexDirection: 'row'
	}
});