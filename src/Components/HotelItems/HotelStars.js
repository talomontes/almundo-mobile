import React, { Component} from 'react';
import { View, Image, FlatList, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class HotelStars extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nuStars: []
		}
	}
	componentWillUpdate(nextProps, nextState){
		if(this.state.nuStars.length != nextProps.hotelStars){
			this.setState({nuStars:new Array(nextProps.hotelStars) });
			console.log("estrellitas",this.state);
		}

	}
	render() {
		return (
			<View>
				<FlatList
					data={this.state.nuStars}
					renderItem={({ item, index }) =>
						<Icon 
							name="star"
							size={20}
							color="yellow"
							style={styles.stars}
						/>
					}
					keyExtractor={(item, index) => index}
					style={styles.starsList}
				/>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	starsList: {
		flex:1,
		flexDirection: 'row',
		width:'100%'
	},
	stars:{
		margin: 2
	}
	
});

export { HotelStars };