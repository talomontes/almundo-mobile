import React, { Component } from 'react'
import { View, Image, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';


export default class HotelStars extends Component {
	constructor(props) {
		super(props);
		this.state = {
			arServices: this.props.arServices
		}
	}
	render() {
		return (
			<View>
				<FlatList
					data={this.state.arServices}
					renderItem={({ item, index }) =>
						<Icon
							name={item}
							size={30}
							color="#900"
						/>
					}
					keyExtractor={(item, index) => index}
				/>
			</View>
		);
	}
}

export { HotelStars };